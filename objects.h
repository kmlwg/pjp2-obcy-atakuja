typedef struct player {
	int score;
	int lives;
	bool live[5];
} player;

typedef struct ship {
	int ship_w_1;	//witth of the first segment of ship
	int ship_w_2;	//width of the second
	int ship_w_3;	//width of the third
	int ship_h_1;	//height of the first segment
	int ship_h_2;	//height of the second and third

	int cannon_h;	//height of the last segment
	int cannon_w;	//width of the last

	float ship_x_1;	//x coordinante of the first segment
	float ship_x_2;	//x coordinante of the second
	float ship_x_3;	//x coordinante of the third
	float cannon_x; // of the last

	float ship_y;
	float ship_dx;

} ship;

typedef struct bullet {
	int x;
	int y;

	int radius;

	float dy;

	bool live;

} bullet;

typedef struct enemy_bullet {
	int x;
	int y;

	int w;
	int h;

	float dy;

	int owner_y;
	int owner_x;

	bool live;
} enemy_bullet;

typedef struct enemy {
	int x;
	int y;
	int w;
	int h;
	int space;

	bool live;
	bool fire;

	int value;
	int border_counter;

	float dx;
} enemy;

