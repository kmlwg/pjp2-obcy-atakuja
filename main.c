//|---------------------------|
//| Space invaders style game |
//| Author: Kamil Giedrys     |
//|---------------------------|

#pragma warning(disable : 4996)
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>
#include "objects.h"


//######################### GLOBALS #############################

int			SCREEN_W = 640;
int			SCREEN_H = 480;
const float FPS = 60;
const int   NUM_BULLETS = 5;

enum MYKEYS {
	KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT, KEY_SPACE, KEY_ENTER
};

bool key[6] = { false, false, false, false, false, false };

int init_allegro_addons(void);

//####################### PROTOTYPES #############################

void  read_score_file(int *highscore);
void write_score_file(int *highscore);

void init_enemy(enemy enemies[6][10], enemy *red_invader);
void draw_enemy(enemy enemies[6][10], ALLEGRO_BITMAP *invaders, enemy *red_invader);
void enemy_change_dx(enemy enemies[6][10]);
void enemy_move_x(enemy enemies[6][10], enemy *red_invader, int *enemy_counter);
void enemy_move_y(enemy enemies[6][10]);

void init_enemy_bullet(enemy_bullet bullets[], int size);
void draw_enemy_bullet(enemy_bullet bullets[], int size);
void fire_enemy_bullet(enemy_bullet bullets[], int size, enemy enemies[6][10]);
void update_enemy_bullet(enemy_bullet bullets[], int size, enemy enemies[6][10]);
void collision_enemy_bullet(enemy_bullet bullets[], int size, ship ship, enemy enemies[6][10],player *player);

void init_player(player *player);
void draw_lives(player *player);

void ship_init(ship *ship);
void draw_ship(ship *ship);

void init_bullet(bullet bullets[], int size);
void draw_bullet(bullet bullets[], int size);
void fire_bullet(bullet bullets[], int size, struct ship *ship);
void update_bullet(bullet bullets[], int size);
void bullet_collision(player *player, bullet bullets[], enemy enemies[6][10], enemy *red_invader, int *enemy_counter);
void reset_bullets(bullet bullets[]);

void draw_main_screen(ALLEGRO_FONT *font, player *player, int *highscore, ALLEGRO_BITMAP *background, int *level);


//####################### MENU ##################################

void menu(int *highscore);
void draw_help(ALLEGRO_EVENT_QUEUE *event_queue, bool *redraw);
void draw_credits(ALLEGRO_EVENT_QUEUE *event_queue, bool *redraw);

void game(ALLEGRO_EVENT_QUEUE *event_queue,int *highscore, bool *redraw);

void check_result(player *player, enemy enemies[6][10], bool *game, ship *ship, bool *redraw, ALLEGRO_EVENT_QUEUE *event_queue, enemy *red_invader,
	bullet bullets[], int *level);
void draw_game_over(ALLEGRO_EVENT_QUEUE *event_queue, bool *redraw);
void draw_game_win(ALLEGRO_EVENT_QUEUE *event_queue, bool *redraw);


int main(void) {

	int highscore;

	srand(time(NULL));

	init_allegro_addons();

	menu(&highscore);

	return 0;
}

void  read_score_file(int *highscore) {
	FILE *file;
	file = fopen("highscore.txt", "r");
	fscanf(file, "%i", highscore);
	fclose(file);
}
void write_score_file(int *highscore) {
	FILE *file;
	file = fopen("highscore.txt", "w");
	fprintf(file, "%i", *highscore);
	fclose(file);
}

int init_allegro_addons() {
	// initializing allegro
	if (!al_init()) {
		fprintf(stderr, "Failed to initialize allegro!\n");
		return -1;
	}

	// initializing allegro addons
	if (!al_init_primitives_addon()) {
		fprintf(stderr, "Failed to initialize primitives addon\n");
		return -1;
	}

	if (!al_init_font_addon()) {
		fprintf(stderr, "Failed to initile font addon\n");
		return -1;
	}

	if (!al_init_ttf_addon()) {
		fprintf(stderr, "failed to initialize ttf addon\n");
		return -1;
	}

	if (!al_init_image_addon()) {
		fprintf(stderr, "failed to initialize image addon\n");
	}

	// installing keyboard
	if (!al_install_keyboard()) {
		fprintf(stderr, "Failed to initialize the keyboard!\n");
		return -1;
	}
}

void init_enemy(enemy enemies[6][10], enemy *red_invader) {
	int x = 100;
	int y = 100;
	int width = 0.048 * SCREEN_W;
	int height = 0.042 * SCREEN_H;
	int space = 0.009 * SCREEN_H;

	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 10; j++) {
			enemies[i][j].live = true;
			enemies[i][j].fire = false;

			enemies[i][j].x = x;
			enemies[i][j].y = y;
			enemies[i][j].w = width;
			enemies[i][j].h = height;
			enemies[i][j].space = space;

			enemies[i][j].border_counter = 0;

			enemies[i][j].dx = 2.0;

			if (i == 0)
				enemies[i][j].value = 50;
			else if (i > 0 && i < 3)
				enemies[i][j].value = 25;
			else if (i > 2 && i < 6)
				enemies[i][j].value = 10;

			x += enemies[i][j].w + enemies[i][j].space;
		}
		x = 100;
		y += height + space;
	}

	red_invader->x = 0;
	red_invader->y = 53;
	red_invader->w = width;
	red_invader->h = height;
	red_invader->live = false;
	red_invader->fire = false;
	red_invader->value = 300;
	red_invader->border_counter = 0;
	red_invader->dx = 5.0;

}
void init_enemy_again(enemy enemies[6][10], enemy *red_invader) {
	int x = 100;
	int y = 100;
	int width = 0.048 * SCREEN_W;
	int height = 0.042 * SCREEN_H;
	int space = 0.009 * SCREEN_H;

	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 10; j++) {
			enemies[i][j].live = true;
			enemies[i][j].fire = false;

			enemies[i][j].x = x;
			enemies[i][j].y = y;

			enemies[i][j].border_counter = 0;

			enemies[i][j].dx = enemies[i][j].dx + 0.5;
			x += enemies[i][j].w + enemies[i][j].space;
		}
		x = 100;
		y += height + space;
	}

	red_invader->x = 0;
	red_invader->y = 53;
	red_invader->live = false;
	red_invader->fire = false;
	red_invader->border_counter = 0;
	red_invader->dx += 5.0;

}

void draw_enemy(enemy enemies[6][10], ALLEGRO_BITMAP *invaders, enemy *red_invader) {
	for (int i = 0; i < 6; i++) {
		if (i == 0) {
			for (int j = 0; j < 10; j++) {
				if (enemies[i][j].live == true) {
					al_draw_scaled_bitmap(invaders, 0 , 321, 
						al_get_bitmap_width(invaders), 160,
						enemies[i][j].x, enemies[i][j].y, enemies[i][j].w, enemies[i][j].h, 0);
				}
			}

		}
		else if (i > 0 && i < 3) {
			for (int j = 0; j < 10; j++) {
				if (enemies[i][j].live == true) {
					al_draw_scaled_bitmap(invaders, 0, 161,
						al_get_bitmap_width(invaders), 160,
						enemies[i][j].x, enemies[i][j].y, enemies[i][j].w, enemies[i][j].h, 0);
				}
			}
		}
		else if (i > 2 && i < 6) {
			for (int j = 0; j < 10; j++) {
				if (enemies[i][j].live == true) {
					al_draw_scaled_bitmap(invaders, 0, 0,
						al_get_bitmap_width(invaders), 160,
						enemies[i][j].x, enemies[i][j].y, enemies[i][j].w, enemies[i][j].h, 0);
				}
			}
		}
	}
	if (red_invader->live) {
		al_draw_scaled_bitmap(invaders, 0, 481,
			al_get_bitmap_width(invaders), 160,
			red_invader->x, red_invader->y, red_invader->w, red_invader->h, 0);
	}
}
void enemy_change_dx(enemy enemies[6][10]) {
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 10; j++) {
			enemies[i][j].dx *= (-1);
		}
	}
}
void red_invader_move_x(enemy *red_invader) {
	if (red_invader->live) {
		red_invader->dx *= (-1);
	}
}
void enemy_move_x(enemy enemies[6][10], enemy *red_invader, int *enemy_counter) {
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 10; j++) {
			enemies[i][j].x += enemies[i][j].dx;
		}
	}
	if (red_invader->live) {
		red_invader->x += red_invader->dx;
	}
	if (*enemy_counter == 59) {
		int p, q;
		for (p = 0; p < 6; p++) {
			for (q = 0; q < 10; q++) {
				enemies[p][q].dx = 5.0;
				*enemy_counter += 1;
				printf("%i\n", *enemy_counter);
			}
		}
	}
}
void enemy_move_y(enemy enemies[6][10]) {
	if (enemies[0][0].border_counter == 2) {
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 10; j++) {
				enemies[i][j].y += enemies[0][0].h + enemies[0][0].space;
			}
		}
		enemies[0][0].border_counter = 0;
	}
}
void init_enemy_bullet(enemy_bullet bullets[], int size) {
	for (int i = 0; i < size; i++) {
		bullets[i].live = false;
		bullets[i].dy = -0.008 * SCREEN_H;
		bullets[i].w = 8;
		bullets[i].h = 16;
	}
}
void draw_enemy_bullet(enemy_bullet bullets[], int size) {
	for (int i = 0; i < size; i++) {
		if (bullets[i].live) {
			al_draw_filled_rectangle(bullets[i].x, bullets[i].y,
				bullets[i].x + bullets[i].w, bullets[i].y + bullets[i].h,
				al_map_rgb(255, 0, 255));
		}
	}
}
void fire_enemy_bullet(enemy_bullet bullets[], int size, enemy enemies[6][10]) {
	for (int i = 0; i < size; i++) {
		if (!bullets[i].live) {
			int y = rand() % 6;
			int x = rand() % 10;

			if (!enemies[y][x].fire && enemies[y][x].live) {
				enemies[y][x].fire = true;
				bullets[i].live = true;

				bullets[i].owner_x = x;
				bullets[i].owner_y = y;

				bullets[i].x = enemies[y][x].x + enemies[y][x].w / 2;
				bullets[i].y = enemies[y][x].y + enemies[y][x].h;
				break;
			}
		}
	}
}
void update_enemy_bullet(enemy_bullet bullets[], int size, enemy enemies[6][10]) {
	for (int i = 0; i < size; i++) {
		if (bullets[i].live) {
			bullets[i].y -= bullets[i].dy;
			if (bullets[i].y >= SCREEN_H) {
				bullets[i].live = false;
				enemies[bullets[i].owner_y][bullets[i].owner_x].fire = false;

			}
		}
	}
}
void collision_enemy_bullet(enemy_bullet bullets[], int size, ship ship, enemy enemies[6][10], player *player) {
	for (int i = 0; i < size; i++) {
		if (bullets[i].live) {
			if (bullets[i].x >= ship.ship_x_1 &&
				bullets[i].x <= ship.ship_x_1 + ship.ship_w_1 &&
				bullets[i].y >= ship.ship_y - ship.ship_h_1 - 2 * ship.ship_h_2) {
				bullets[i].live = false;
				enemies[bullets[i].owner_y][bullets[i].owner_x].fire = false;
				if (player->lives >= 0) {
					player->live[player->lives] = false;
					player->lives -= 1;
				}
			}
		}
	}
}

void init_player(player *player) {
	for (int i = 0; i < 5; i++) {
		player->live[i] = true;
	}

	player->score = 0;
	player->lives = 4;
}
void draw_lives(player *player) {
	const int space = 4;
	int x_1 = 0.63125 * SCREEN_W;
	const int y_1 = 30;
	const int w = 15;
	const int h = 8;

	for (int i = 0; i < 5; i++) {
		if (player->live[i] == true) {
			al_draw_filled_rectangle(x_1, y_1, x_1 + w, y_1 + h,
				al_map_rgb(0, 255, 0));
		}
		x_1 += space + w;
	}
}

void ship_init(ship *ship) {

	ship->ship_w_1 = 0.08 * SCREEN_W;
	ship->ship_w_2 = 0.05 * SCREEN_W;
	ship->ship_w_3 = 0.02 * SCREEN_W;
	ship->ship_h_1 = 0.02 * SCREEN_H;
	ship->ship_h_2 = 0.01 * SCREEN_H;

	ship->cannon_h = 0.015 * SCREEN_H;
	ship->cannon_w = 0.005 * SCREEN_W;

	ship->ship_x_1 = SCREEN_W / 2.0 - ship->ship_w_1 / 2.0;
	ship->ship_x_2 = SCREEN_W / 2.0 - ship->ship_w_2 / 2.0;
	ship->ship_x_3 = SCREEN_W / 2.0 - ship->ship_w_3 / 2.0;
	ship->cannon_x = SCREEN_W / 2.0 - ship->cannon_w / 2.0;

	ship->ship_y = SCREEN_H - ship->ship_h_1 - 5.0;

	ship->ship_dx = 15.0;
}
void draw_ship(ship *ship) {
	al_draw_filled_rectangle(ship->ship_x_1, ship->ship_y,
		ship->ship_x_1 + ship->ship_w_1, ship->ship_y + ship->ship_h_1,
		al_map_rgb(0, 255, 0));
	al_draw_filled_rectangle(ship->ship_x_2, ship->ship_y - ship->ship_h_2,
		ship->ship_x_2 + ship->ship_w_2, ship->ship_y,
		al_map_rgb(0, 255, 0));

	al_draw_filled_rectangle(ship->ship_x_3, ship->ship_y - 2 * ship->ship_h_2,
		ship->ship_x_3 + ship->ship_w_3, ship->ship_y - ship->ship_h_2,
		al_map_rgb(0, 255, 0));

	al_draw_filled_rectangle(ship->cannon_x, ship->ship_y - 2 * ship->ship_h_2 - ship->cannon_h,
		ship->cannon_x + ship->cannon_w, ship->ship_y - 2 * ship->ship_h_2,
		al_map_rgb(0, 255, 0));


}

void init_bullet(bullet bullets[], int size) {
	for (int i = 0; i < size; i++) {
		bullets[i].dy = 0.007 * SCREEN_H;
		bullets[i].live = false;
		bullets[i].radius = 0.004 * SCREEN_W;
	}
}
void draw_bullet(bullet bullets[], int size) {
	for (int i = 0; i < size; i++) {
		if (bullets[i].live) {
			al_draw_filled_circle(bullets[i].x, bullets[i].y, bullets[i].radius,
				al_map_rgb(0, 255, 0));
		}
	}
}
void fire_bullet(bullet bullets[], int size, struct ship *ship) {
	for (int i = 0; i < size; i++) {
		if (!bullets[i].live) {
			bullets[i].x = ship->cannon_x + ship->cannon_w / 2;
			bullets[i].y = ship->ship_y - ship->ship_h_1 - 2 * ship->ship_h_2 - ship->cannon_h;
			bullets[i].live = true;
			break;
		}
	}
}
void update_bullet(bullet bullets[], int size) {
	for (int i = 0; i < size; i++) {
		if (bullets[i].live) {
			bullets[i].y -= bullets[i].dy;
			if (bullets[i].y < 52) {
				bullets[i].live = false;
			}
		}
	}
}
void bullet_collision(player *player, bullet bullets[], enemy enemies[6][10], enemy *red_invader, int *enemy_counter) {
	for (int i = 0; i < 6; i++) {
		if (bullets[i].live) {
			for (int j = 5; j >= 0; j--) {
				for (int k = 0; k < 10; k++) {
					if (enemies[j][k].live) {
						if (bullets[i].y + bullets[i].radius <= enemies[j][k].y + enemies[i][k].h &&
							bullets[i].y - bullets[i].radius >= enemies[j][k].y &&
							bullets[i].x - bullets[i].radius >= enemies[j][k].x  &&
							bullets[i].x + bullets[i].radius <= enemies[j][k].x + enemies[j][k].w)
						{
							bullets[i].live = false;
							enemies[j][k].live = false;
							*enemy_counter += 1;
							printf("%i\n", *enemy_counter);
							player->score += enemies[j][k].value;
						}
					}
				}
				if (red_invader->live) {
					if (bullets[i].y + bullets[i].radius <= red_invader->y + red_invader->h &&
						bullets[i].y - bullets[i].radius >= red_invader->y &&
						bullets[i].x - bullets[i].radius >= red_invader->x  &&
						bullets[i].x + bullets[i].radius <= red_invader->x + red_invader->w)
					{
						bullets[i].live = false;
						red_invader->live = false;
						player->score += red_invader->value;
					}
				}
			}
		}
	}
}
void reset_bullets(bullet bullets[]) {
	for (int i = 0; i < 6; i++) {
		bullets[i].live = false;
	}
}

void draw_main_screen(ALLEGRO_FONT *font, player *player, int *highscore, ALLEGRO_BITMAP *background, int *level) {

	const int y_1 = 4;
	const int y_2 = 25;

	al_draw_filled_rectangle(0, 50, SCREEN_W, 52,
		al_map_rgb(0, 255, 0));
	al_draw_text(font, al_map_rgb(0, 255, 0), 4, y_1, 0,
		"SCORE");
	al_draw_textf(font, al_map_rgb(0, 255, 0), 4, y_2, 0,
		"%i", player->score);
	al_draw_text(font, al_map_rgb(0, 255, 0), 0.24067 * SCREEN_W, y_1, 0,
		"LEVEL");
	al_draw_textf(font, al_map_rgb(0, 255, 0), 0.24067 * SCREEN_W, y_2, 0,
		"%i",*level);
	al_draw_text(font, al_map_rgb(0, 255, 0), 0.63125 * SCREEN_W, y_1, 0,
		"LIVES");
	draw_lives(player);
	al_draw_text(font, al_map_rgb(0, 255, 0), 0.84688 * SCREEN_W, y_1, 0,
		"HIGHSCORE");
	al_draw_textf(font, al_map_rgb(0, 255, 0), 0.84688 * SCREEN_W, y_2, 0,
		"%i", *highscore);
	al_draw_scaled_bitmap(background, 0, 0, al_get_bitmap_width(background), al_get_bitmap_height(background),
		0, 52, SCREEN_W, SCREEN_H - 52, 0);

}

//####################### MENU ##################################

void menu(int *highscore) {

	ALLEGRO_DISPLAY		*display = NULL;
	ALLEGRO_DISPLAY_MODE disp_data;
	ALLEGRO_TIMER		*timer = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;

	int option = 0;
	bool confirm = false;
	bool redraw = true;

	ALLEGRO_BITMAP *new_game = al_load_bitmap("menu_newgame.bmp");
	ALLEGRO_BITMAP *help = al_load_bitmap("menu_help.bmp");
	ALLEGRO_BITMAP *credits = al_load_bitmap("menu_credits.bmp");
	ALLEGRO_BITMAP *exit = al_load_bitmap("menu_exit.bmp");

	al_get_display_mode(al_get_num_display_modes() - 1, &disp_data);

	al_set_new_display_flags(ALLEGRO_FULLSCREEN);

	SCREEN_W = disp_data.width;
	SCREEN_H = disp_data.height;

	display = al_create_display(SCREEN_W, SCREEN_H);
	if (!display) {
		fprintf(stderr, "Failed to create display!\n");
		return -1;
	}

	al_set_target_bitmap(al_get_backbuffer(display));

	timer = al_create_timer(1.0 / FPS);
	if (!timer) {
		fprintf(stderr, "Failed to create timer!\n");
		return -1;
	}

	event_queue = al_create_event_queue();
	if (!event_queue) {
		fprintf(stderr, "Failed to create event_queue!\n");
		al_destroy_timer(timer);
		return -1;
	}

	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());

	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_flip_display();

	al_start_timer(timer);

	bool main_loop = true;

	while (main_loop) {
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);

		if (ev.type == ALLEGRO_EVENT_TIMER) {
			if (option > 3)
				option = 0;
			if (option < 0)
				option = 3;
			else if (key[KEY_ENTER] && option == 0) {
				game(event_queue, highscore, &redraw);
			}
			else if (key[KEY_ENTER] && option == 1) {
				draw_help(event_queue, &redraw);
			}
			else if (key[KEY_ENTER] && option == 2) {
				draw_credits(event_queue, &redraw);
			}
			else if (key[KEY_ENTER] && option == 3) {
				break;
			}
			redraw = true;
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_UP:
				key[KEY_UP] = true;
				option--;
				printf("socoso\n");
				break;
			case ALLEGRO_KEY_DOWN:
				key[KEY_DOWN] = true;
				option++;
				break;
			case ALLEGRO_KEY_ENTER:
				key[KEY_ENTER] = true;
				break;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_UP) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_UP:
				key[KEY_UP] = false;
				break;
			case ALLEGRO_KEY_DOWN:
				key[KEY_DOWN] = false;
				break;
			case ALLEGRO_KEY_ENTER:
				key[KEY_ENTER] = false;
				break;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
			main_loop = false;
		}

		if (redraw && al_is_event_queue_empty(event_queue)) {
			redraw = false;

			al_clear_to_color(al_map_rgb(0, 0, 0));
			
			switch (option) {
			case 0:
				al_draw_scaled_bitmap(new_game, 0, 0, al_get_bitmap_width(new_game), al_get_bitmap_height(new_game),
					0, 0, SCREEN_W, SCREEN_H, 0);
				break;
			case 1:
				al_draw_scaled_bitmap(help, 0, 0, al_get_bitmap_width(help), al_get_bitmap_height(help),
					0, 0, SCREEN_W, SCREEN_H, 0);
				break;
			case 2:
				al_draw_scaled_bitmap(credits, 0, 0, al_get_bitmap_width(credits), al_get_bitmap_height(credits),
					0, 0, SCREEN_W, SCREEN_H, 0);
				break;
			case 3:
				al_draw_scaled_bitmap(exit, 0, 0, al_get_bitmap_width(exit), al_get_bitmap_height(exit),
					0, 0, SCREEN_W, SCREEN_H, 0);
				break;
			}
			al_flip_display();
		}
	}
	al_destroy_bitmap(new_game);
	al_destroy_bitmap(help);
	al_destroy_bitmap(credits);
	al_destroy_bitmap(exit);

	al_destroy_timer(timer);
	al_destroy_display(display);
	al_destroy_event_queue(event_queue);
}

void draw_help(ALLEGRO_EVENT_QUEUE *event_queue, bool *redraw) {
	ALLEGRO_BITMAP *help_screen = al_load_bitmap("help.bmp");
	
	al_flip_display();
	bool live = true;
	while (live) {
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_TIMER) {
			*redraw = true;
		}
		if (ev.type == ALLEGRO_EVENT_KEY_UP) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_ENTER:
				key[KEY_ENTER] = false;
				break;
			}
		}
		if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_ENTER:
				key[KEY_ENTER] = false;
				live = false;
				break;
			}
		}
		if (*redraw && al_is_event_queue_empty(event_queue)) {
			*redraw = false;

			al_draw_scaled_bitmap(help_screen, 0, 0, 1920, 1080, 0, 0, SCREEN_W, SCREEN_H, 0);
			al_flip_display();
		}
	}
	al_destroy_bitmap(help_screen);
}
void draw_credits(ALLEGRO_EVENT_QUEUE *event_queue, bool *redraw) {
	ALLEGRO_BITMAP *credits_screen = al_load_bitmap("credits.bmp");

	al_flip_display();
	bool live = true;
	while (live) {
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_TIMER) {
			*redraw = true;
		}
		if (ev.type == ALLEGRO_EVENT_KEY_UP) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_ENTER:
				key[KEY_ENTER] = false;
				break;
			}
		}
		if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_ENTER:
				key[KEY_ENTER] = false;
				live = false;
				break;
			}
		}
		if (*redraw && al_is_event_queue_empty(event_queue)) {
			*redraw = false;

			al_draw_scaled_bitmap(credits_screen, 0, 0, 1920, 1080, 0, 0, SCREEN_W, SCREEN_H, 0);
			al_flip_display();
		}
	}
	al_destroy_bitmap(credits_screen);
}

void game( ALLEGRO_EVENT_QUEUE *event_queue, int *highscore, bool *redraw) {

	ship ship;
	player player;
	bullet player_bullets[5];
	enemy enemies[6][10];
	enemy red_invader;
	enemy_bullet enemy_bullets[5];

	int level = 1;
	int my_timer = 0;
	int enemy_counter = 0;

	bool game = true;

	ship_init(&ship);
	init_player(&player);
	init_enemy(enemies, &red_invader);
	init_bullet(player_bullets, NUM_BULLETS);
	init_enemy_bullet(enemy_bullets, NUM_BULLETS);

	ALLEGRO_BITMAP *invaders     = al_load_bitmap("invaders.bmp");
	ALLEGRO_BITMAP *background   = al_load_bitmap("background.jpg");

	al_convert_mask_to_alpha(invaders, al_map_rgb(255, 255, 255));

	ALLEGRO_FONT *font_default = al_load_ttf_font("SourceCodePro-Bold.ttf", 16, 0);

	read_score_file(highscore);

	if (!font_default) {
		fprintf(stderr, "Could not load 'SourceCodePro-Bold.ttf'.\n");
		al_destroy_font(font_default);
		return -1;
	}

	*redraw = true;

	al_flip_display();

	while (game) {
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);


		if (enemy_counter == 120) {
		
		}
		if (my_timer == 300) {
			red_invader.live = true;
		}
		if (red_invader.live) {
			if (my_timer > 20*60) {
				red_invader.live = false;
			}
		}
		if (ev.type == ALLEGRO_EVENT_TIMER) {

			// movement of ship/player
			if (key[KEY_LEFT] && ship.ship_x_1 >= 0) {
				ship.ship_x_1 -= ship.ship_dx;
				ship.ship_x_2 -= ship.ship_dx;
				ship.ship_x_3 -= ship.ship_dx;
				ship.cannon_x -= ship.ship_dx;
			}
			else if (key[KEY_RIGHT] && (ship.ship_x_1 + ship.ship_w_1) <= SCREEN_W) {
				ship.ship_x_1 += ship.ship_dx;
				ship.ship_x_2 += ship.ship_dx;
				ship.ship_x_3 += ship.ship_dx;
				ship.cannon_x += ship.ship_dx;

			}
			if (enemies[0][0].x <= 0 || (enemies[0][9].x + enemies[0][9].w + enemies[0][9].space) >= SCREEN_W) {
				enemy_change_dx(enemies);
				enemies[0][0].border_counter += 1;
			}
			if (red_invader.x < 0 || red_invader.x + red_invader.w >= SCREEN_W) {
				red_invader_move_x(&red_invader);
			}
			bullet_collision(&player, player_bullets, enemies, &red_invader, &enemy_counter);
			collision_enemy_bullet(enemy_bullets, NUM_BULLETS, ship, enemies, &player);

			enemy_move_x(enemies, &red_invader, &enemy_counter);
			enemy_move_y(enemies);

			fire_enemy_bullet(enemy_bullets, NUM_BULLETS, enemies);
			
			update_bullet(player_bullets, NUM_BULLETS);
			update_enemy_bullet(enemy_bullets, NUM_BULLETS, enemies);



			if (player.score >= *highscore) {
				*highscore = player.score;
				write_score_file(highscore);
			}

			*redraw = true;
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_UP:
				key[KEY_UP] = true;
				break;

			case ALLEGRO_KEY_DOWN:
				key[KEY_DOWN] = true;
				break;

			case ALLEGRO_KEY_LEFT:
				key[KEY_LEFT] = true;
				break;

			case ALLEGRO_KEY_RIGHT:
				key[KEY_RIGHT] = true;
				break;
			case ALLEGRO_KEY_SPACE:
				key[KEY_SPACE] = true;
				fire_bullet(player_bullets, NUM_BULLETS, &ship);
				break;

			case ALLEGRO_KEY_ENTER:
				key[KEY_ENTER] = true;
				break;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_UP) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_UP:
				key[KEY_UP] = false;
				break;

			case ALLEGRO_KEY_DOWN:
				key[KEY_DOWN] = false;
				break;

			case ALLEGRO_KEY_LEFT:
				key[KEY_LEFT] = false;
				break;

			case ALLEGRO_KEY_RIGHT:
				key[KEY_RIGHT] = false;
				break;
			case ALLEGRO_KEY_SPACE:
				key[KEY_SPACE] = false;
				break;

			case ALLEGRO_KEY_ENTER:
				key[KEY_ENTER] = false;
				break;
			}
		}

		else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
			game = false;
		}

		if (*redraw && al_is_event_queue_empty(event_queue)) {
			*redraw = false;

			al_clear_to_color(al_map_rgb(0, 0, 0));
			check_result(&player, enemies, &game, &ship, redraw,event_queue, &red_invader, player_bullets, &level);
			draw_main_screen(font_default, &player, highscore, background, &level);
			draw_ship(&ship);

			draw_enemy_bullet(enemy_bullets, NUM_BULLETS);
			draw_bullet(player_bullets, NUM_BULLETS);

			draw_enemy(enemies, invaders, &red_invader);



			al_flip_display();
			my_timer++;
			
		}
	}
	al_destroy_font(font_default);
	al_destroy_bitmap(invaders);
	al_destroy_bitmap(background);
}

void check_result(player *player, enemy enemies[6][10], bool *game, ship *ship, bool *redraw, ALLEGRO_EVENT_QUEUE *event_queue, enemy *red_invader,
	bullet bullets[], int *level) {
	
	bool win = false;
	int count = 0;

	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 10; j++) {
			if (enemies[i][j].live) {
				if (enemies[i][j].y >= SCREEN_H ||
					(enemies[i][j].x >= ship->ship_x_1 &&
						enemies[i][j].x <= ship->ship_x_1 + ship->ship_w_1 &&
						enemies[i][j].y + enemies[i][j].h >= ship->ship_y - 2 * ship->ship_h_2 - ship->cannon_h)) {
					
					draw_game_over(event_queue, redraw);

					*game = false;
				}
			}
			if (!enemies[i][j].live) {
				count+=1;
			}
		}
	}
	if (player->live[0] == false){
		draw_game_over(event_queue, redraw);
		*game = false;

	}
	else if (count == 60) {
		ALLEGRO_BITMAP *game_won = al_load_bitmap("win_screen.bmp");
		al_flip_display();
		draw_game_win(event_queue, redraw);
		*game = true;
		init_enemy_again(enemies, red_invader);
		reset_bullets(bullets);
		*level += 1;
		al_destroy_bitmap(game_won);
	}
	count = 0;
}

void draw_game_over(ALLEGRO_EVENT_QUEUE *event_queue, bool *redraw) {
	ALLEGRO_BITMAP *game_over = NULL;
	game_over = al_load_bitmap("game_over.bmp");
	if (game_over == NULL)
		printf("boom\n");

	al_flip_display();
	bool live = true;

	key[KEY_LEFT] = false;
	key[KEY_RIGHT] = false;


	while (live) {
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_TIMER) {
			*redraw = true;
		}
		if (ev.type == ALLEGRO_EVENT_KEY_UP) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_ENTER:
				key[KEY_ENTER] = false;
				break;
			}
		}
		if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_ENTER:
				key[KEY_ENTER] = false;
				live = false;
				break;
			}
		}
		if (*redraw && al_is_event_queue_empty(event_queue)) {
			*redraw = false;

			al_draw_scaled_bitmap(game_over, 0, 0, 1920,
				1080, 0, 0, SCREEN_W, SCREEN_H, 0);
			al_flip_display();
		}

	}
	al_destroy_bitmap(game_over);
}

void draw_game_win(ALLEGRO_EVENT_QUEUE *event_queue, bool *redraw) {
	ALLEGRO_BITMAP *game_win = NULL;
	game_win = al_load_bitmap("win_screen.bmp");
	if (game_win == NULL)
		printf("boom\n");

	al_flip_display();
	bool live = true;

	key[KEY_LEFT] = false;
	key[KEY_RIGHT] = false;


	while (live) {
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_TIMER) {
			*redraw = true;
		}
		if (ev.type == ALLEGRO_EVENT_KEY_UP) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_ENTER:
				key[KEY_ENTER] = false;
				break;
			}
		}
		if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_ENTER:
				key[KEY_ENTER] = false;
				live = false;
				break;
			}
		}
		if (*redraw && al_is_event_queue_empty(event_queue)) {
			*redraw = false;

			al_draw_scaled_bitmap(game_win, 0, 0, 1920,
				1080, 0, 0, SCREEN_W, SCREEN_H, 0);
			al_flip_display();
		}

	}
	al_destroy_bitmap(game_win);
}